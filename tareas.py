#!/usr/bin/env python3

import math
import operator
from functools import reduce
from collections import OrderedDict
from pprint import pprint


def prod(iterable):
    """Devuelve el producto de los elementos en iterable."""
    return reduce(operator.mul, iterable, 1)


def gcd(a, b):
    """Devuelve el máximo común divisor entre a y b."""
    if not b:
        return a
    return gcd(b, a % b)


def lcm(a, b):
    """Devuelve el mínimo común múltiplo entre a y b."""
    return (a * b) / gcd(a, b)


class RateMonotonic:
    def __init__(self, sistemaTareas):
        self.sistemaTareas = sistemaTareas
        self.tareas = self.sistemaTareas.tareas
        self.hiperperiodo = self.sistemaTareas.hiperperiodo
        self._tiemposRespuesta = []

    def ordenaTareasPeriodo(tareas):
        return sorted(tareas, key=lambda tarea: tarea.periodo)

    def _sortTareasPeriodo(self):
        return RateMonotonic.ordenaTareasPeriodo(self.tareas)

    def cargaTrabajo(tareas, t):
        """Carga de trabajo de las tareas en el instante de tiempo t"""
        return sum(tarea.cargaTrabajo(t) for tarea in tareas)

    def listaEjecucionTareas(tareas, ticks):
        listaEjecucion = []
        ejecutar = [True] * len(tareas)
        tick = 0
        tickProximo = 0
        while tick < ticks:
            while tick < tickProximo:
                tick += 1
                ejecutar = [
                    ejecutar[i] or tick % tarea.periodo == 0
                    for i, tarea in enumerate(tareas)
                ]
                 
            t = False
            for i, e in enumerate(ejecutar):
                if e:
                    t = True
                    ejecutar[i] = False
                    listaEjecucion.extend(
                        (i,) * tareas[i].duracion
                    )
                    tickProximo = tick + tareas[i].duracion
                    break
            if not t:
                listaEjecucion.append(None)
                tickProximo = tick + 1

        return listaEjecucion

    def listaEjecucion(self, ticks):
        return RateMonotonic.listaEjecucionTareas(self.tareas, ticks)

    def tiemposRespuestaTareas(tareas):
        tiemposRespuesta = [tareas[0].duracion]

        for i, tarea in enumerate(tareas[1::], start=1):
            tiempoAnterior = None
            tiempoActual = tiemposRespuesta[i-1] + tarea.duracion

            # Buscamos punto fijo
            while tiempoAnterior != tiempoActual:
                tiempoAnterior = tiempoActual
                tareasAnteriores = tareas[0:i]
                cargaTrabajo = RateMonotonic.cargaTrabajo(
                    tareasAnteriores, tiempoActual)
                tiempoActual = tarea.duracion + cargaTrabajo

                if tiempoActual > tarea.vencimiento:
                    raise ValueError(
                        'Tiempo de respuesta {} mayor al vencimiento {}'
                        .format(tiempoActual, tarea.vencimiento)
                    )

            tiemposRespuesta.append(tiempoActual)

        return tiemposRespuesta

    @property
    def tiemposRespuesta(self):
        if self._tiemposRespuesta:
            return self._tiemposRespuesta

        tareas = self._sortTareasPeriodo()
        self._tiemposRespuesta = RateMonotonic.tiemposRespuestaTareas(tareas)
        return self._tiemposRespuesta

    def ranuraVacia(self, n=1):
        tareas = self._sortTareasPeriodo()
        tareas.append(Tarea(n, 1, self.hiperperiodo+n))
        return RateMonotonic.tiemposRespuestaTareas(tareas)[-1]


class Tarea:
    """Una tarea de un sistema"""

    def __init__(self, duracion, periodo, vencimiento=None):
        self.duracion = duracion
        self.periodo = periodo
        self.vencimiento = vencimiento if vencimiento else periodo
        self.factorUtilizacion = duracion / periodo

    def instancias(self, t):
        """Número de instancias que llegaron al instante de tiempo t"""
        return math.ceil(t / self.periodo)

    def cargaTrabajo(self, t):
        """Carga de trabajo de la tarea actual para el instante de tiempo t"""
        return self.instancias(t) * self.duracion

    def __str__(self):
        return '({},{},{})'.format(
            self.duracion,
            self.periodo,
            self.vencimiento
        )


class SistemaTareas:
    """Sistema de tareas"""

    def __init__(self, tareas=None):
        self.tareas = tareas if tareas else []

    def addTarea(self, tarea):
        self.tareas.append(tarea)

    @property
    def cantTareas(self):
        return len(self.tareas)

    @property
    def factorUtilizacion(self):
        return sum(tarea.factorUtilizacion for tarea in self.tareas)

    @property
    def hiperperiodo(self):
        periodos = (tarea.periodo for tarea in self.tareas)
        return reduce(lcm, periodos, 1)

    @property
    def cotaLiu(self):
        return ((2 ** (1/self.cantTareas) - 1) * self.cantTareas)

    @property
    def cumpleCotaLiu(self):
        return self.factorUtilizacion <= self.cotaLiu

    @property
    def cumpleCotaBini(self):
        return prod(tarea.factorUtilizacion + 1 for tarea in self.tareas) <= 2

    def cargaTrabajo(self, t):
        """Carga de trabajo del sistema para el instante de tiempo t"""
        return sum(cargaTrabajo(tarea) for tarea in self.tareas)

    @property
    def estaSaturado(self):
        """
        Devuelve si el sistema esta saturado.

        Un sistema saturado siempre es planificable solo por planificadores
        óptimos
        """
        self.factorUtilizacion == 1

    @property
    def estaSobresaturado(self):
        """Un sistema sobresaturado no es planificable"""
        return self.factorUtilizacion > 1

    def describe(self):
        hiperperiodo = 'Hiperperiodo: {}'.format(self.hiperperiodo)
        fu = 'Factor de utilización: {}'.format(self.factorUtilizacion)
        cotaLiu = 'Cumple cota de Liu: {}'.format(
            'Sí' if self.cumpleCotaLiu else 'No'
        )
        cotaBini = 'Cumple cota de Bini: {}'.format(
            'Sí' if self.cumpleCotaBini else 'No'
        )

        rateMonotonic = RateMonotonic(self)
        tiemposRespuesta = [
            'Tiempo de respuesta de la tarea {}: {}'
            .format(tarea, tiemposRespuesta)
            for tarea, tiemposRespuesta
            in enumerate(rateMonotonic.tiemposRespuesta, 1)
        ]
        tiemposRespuestaRateMonotonic = '\n'.join(tiemposRespuesta)

        ranura = 'Tiempo de ranura vacía: {}'.format(
            rateMonotonic.ranuraVacia())
        return '\n'.join([
            hiperperiodo,
            fu,
            cotaLiu,
            cotaBini,
            tiemposRespuestaRateMonotonic,
            ranura
        ])

    def __str__(self):
        return ''.join([str(tarea) + ' ' for tarea in self.tareas])
